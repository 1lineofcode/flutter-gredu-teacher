import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gredu_teacher/app/reusable/widgets/_index.dart';


///   created               : Aditya Pratama
///   originalFilename      : ex_progress_test
///   date                  : 14 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///
///
void main() {

  //  TEST 1 : need to create test !!
  testWidgets('finds this widget using a Key', (WidgetTester tester) async {
    const testKey = Key('K');
    await tester.pumpWidget(const ExProgress(key: testKey, value: 10));
    expect(find.byKey(testKey), findsOneWidget);
  });


  // TEST 2
  testWidgets('is widget value in the correct place', (WidgetTester tester) async {
    const valueTest = 80;
    await tester.pumpWidget(ExProgress(value: valueTest));
    expect(valueTest, 80);
  });

  // TEST 3
  testWidgets('case when value be incremented', (WidgetTester tester) async {
    final progress = ExProgress(value: 10);
    var incrementProgressValue = (progress.value + 40);
    expect(incrementProgressValue, 50);
  });

}
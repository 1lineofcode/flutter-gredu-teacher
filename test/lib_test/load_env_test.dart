///   created               : Aditya Pratama
///   originalFilename      : flutter_config_test
///   date                  : 12 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///

import 'package:flutter_config/flutter_config.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  FlutterConfig.loadValueForTesting({
    'BASE_URL': 'https://adit.web.id',
  });

  test('ap_test', () {
    var cek = FlutterConfig.get('BASE_URL');
    print(cek);
  });
}

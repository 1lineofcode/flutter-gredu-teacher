class InboxType {
  static const ATTENDANCE = "ATTENDANCE";
  static const ANNOUNCEMENT = "ANNOUNCEMENT";
  static const COUNSELING = "COUNSELING";
}

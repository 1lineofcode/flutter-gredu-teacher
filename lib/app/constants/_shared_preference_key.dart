import 'package:get_storage/get_storage.dart';

///   created               : Aditya Pratama
///   originalFilename      : _shared_preference_key
///   date                  : 10 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   Persistent key/value storage for Android, iOS, Web, Linux, Mac and Fuchsia and Windows,
///   that combines fast memory access with persistent storage.
///
///   code rules :
///   - start with [PREF_XXX]
///   - UPPERCASE
///   - value : same of key
///   - sort by name (ascending)

// —————————————————————————————————————————————————————————————————————————————
final pref = GetStorage(); // karna factory, slalu pake object ini yaw .
// —————————————————————————————————————————————————————————————————————————————
const PREF_CURRENT_LANGUAGE = 'PREF_CURRENT_LANGUAGE';
const PREF_CURRENT_USER_PROFILE = 'PREF_CURRENT_USER_PROFILE';
const PREF_IS_FIRST_INSTALL = "PREF_IS_FIRST_INSTALL";
const PREF_IS_LOGIN = 'PREF_IS_LOGIN';
const PREF_TOKEN = 'PREF_TOKEN';

/** use example
  void main() {

    // WRITE
    var token = pref.write(PREF_TOKEN, value_from_response_or_anything);

    // READ
    var token = pref.read(PREF_TOKEN);

    // TO REMOVE SOME PREF
    pref.remove(PREF_TOKEN);

    // TO CLEAR ALL PREF
    pref.erase();
  }

 * */
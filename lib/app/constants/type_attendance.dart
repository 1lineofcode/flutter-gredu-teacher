class AttendanceType {
  static const TYPE = {"homeroom": "Harian", "subject": "Setengah Hari"};
  static const SICK = "sick";
  static const PERMISSION = "permission";
  static const PRESENT = "present";
  static const ABSTAIN = "abstain";
  static const ALPHA = "alpha";
  static const LATE = "late";
}

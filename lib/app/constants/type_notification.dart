class NotificationType {
  static const LESSON = "lesson";
  static const ASSESSMENT = "assessment";
  static const INTERACTION = "interaction";
  static const LIBRARY = "library";
  static const COMPLAINT = "complaint";
}

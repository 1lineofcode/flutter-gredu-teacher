class CounselingType {
  CounselingType();
  static ListType typeList = const ListType();
  static CreatorType typeCreator = const CreatorType();
  static StatusType typeStatus = const StatusType();

}

class ListType {
  const ListType();
  String get chat => 'chat';
  String get meeting => 'meeting';
}

class CreatorType {
  const CreatorType();
  String get client => "client";
  String get counselor => "counselor";
  String get custodian => "custodian";
}

class StatusType {
  const StatusType();
  String get started => "started";
  String get ended => "ended";
  String get evaluated => "evaluated";

  Map get statusKV => {
    started : "Dimulai",
    ended : "Menunggu hasil",
    evaluated : "Sesi selesai",
  };
  
}

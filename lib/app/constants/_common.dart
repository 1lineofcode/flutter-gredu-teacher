
///   created               : Aditya Pratama
///   originalFilename      : _common
///   date                  : 13 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   commonly used
///
const BLANK = ' ';
const EMPTY = '';
const DASH = '-';
const NEWLINE = '\n';
const NIL = null;
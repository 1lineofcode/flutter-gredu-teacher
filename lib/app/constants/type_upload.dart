class UploadType {
  static const LIVE_INTERACTION = "live_interaction";
  static const COUNSELING_REPORT = "counseling_report";
  static const LESSON_DOCUMENT = "lesson_document";
  static const LESSON_MULTIMEDIA = "lesson_multimedia";
  static const EXAM_INSTRUCTION = "exam_instruction";
}

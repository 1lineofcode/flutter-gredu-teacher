///   created               : Aditya Pratama
///   originalFilename      : endpoint
///   date                  : 12 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   list endpoint
class Endpoint {

  // —————————————————————————————————————————————————————————————————————————————
  // AUTH
  // —————————————————————————————————————————————————————————————————————————————
  static const LOGIN              = "/authentication/request_token";
  static const LOGOUT             = "/authentication/destroy_token";
  static const REGISTER           = "/authentication/register";
  static const VERIFICATION_EMAIL = "/authentication/verification_email";
  static const ACTIVATE_ACCOUNT   = "/authentication/activate";
  static const FORGOT_PASSWORD    = "/authentication/forgot_password";

  // —————————————————————————————————————————————————————————————————————————————
  // V1
  // —————————————————————————————————————————————————————————————————————————————
  // static const REPLACE_ME = "/v1/assessments/{id}";
  // static const REPLACE_ME = "/v1/attendances/bulk_update";
  // static const REPLACE_ME = "/v1/attendances/index";
  // static const REPLACE_ME = "/v1/attendances/index";
  // static const REPLACE_ME = "/v1/attendances/update";
  // static const REPLACE_ME = "/v1/filters/attitude_aspects";
  // static const REPLACE_ME = "/v1/filters/attitude_scores";
  // static const REPLACE_ME = "/v1/filters/classes";
  // static const REPLACE_ME = "/v1/filters/classes";
  // static const REPLACE_ME = "/v1/filters/grades";
  // static const REPLACE_ME = "/v1/filters/lessons";
  // static const REPLACE_ME = "/v1/filters/room";
  // static const REPLACE_ME = "/v1/filters/subject_classes";
  // static const REPLACE_ME = "/v1/filters/subjects";
  // static const REPLACE_ME = "/v1/filters/users";
  // static const REPLACE_ME = "/v1/lessons";
  // static const REPLACE_ME = "/v1/lessons/{id}";
  // static const REPLACE_ME = "/v1/lessons/{id}/comments";
  // static const REPLACE_ME = "/v1/lessons/{id}/comments";
  // static const REPLACE_ME = "/v1/lessons/{id}/comments/{comment_id}";
  // static const REPLACE_ME = "/v1/schools/list";
  // static const REPLACE_ME = "/v1/scores/index";
  // static const REPLACE_ME = "/v1/scores/{user_id}";
  // static const REPLACE_ME = "/v1/scores/{user_id}/assessment/{assessment_id}";
  // static const REPLACE_ME = "/v1/users";
  // static const REPLACE_ME = "/v1/users/change_password";

  // —————————————————————————————————————————————————————————————————————————————
  // V2
  // —————————————————————————————————————————————————————————————————————————————
  // static const REPLACE_ME = "/v2/address";
  // static const REPLACE_ME = "/v2/address";
  // static const REPLACE_ME = "/v2/assessments/attitudes";
  // static const REPLACE_ME = "/v2/assessments/create/";
  // static const REPLACE_ME = "/v2/attendances/bulk_update";
  // static const REPLACE_ME = "/v2/attendances/calendars";
  // static const REPLACE_ME = "/v2/counselings";
  // static const REPLACE_ME = "/v2/counselings";
  // static const REPLACE_ME = "/v2/counselings/{id}";
  // static const REPLACE_ME = "/v2/counselings/{id}";
  // static const REPLACE_ME = "/v2/counselings/{id}";
  // static const REPLACE_ME = "/v2/counselings/{id}/end";
  // static const REPLACE_ME = "/v2/counselings/{id}/report";
  // static const REPLACE_ME = "/v2/inboxs/user";
  // static const REPLACE_ME = "/v2/inboxs/user/{uid}";
  // static const REPLACE_ME = "/v2/lessons/{lessonId}/acknowledgements";
  // static const REPLACE_ME = "/v2/lessons/{lessonId}/students";
  // static const REPLACE_ME = "/v2/messages";
  // static const REPLACE_ME = "/v2/messages/count";
  // static const REPLACE_ME = "/v2/messages/intents";
  // static const REPLACE_ME = "/v2/messages/{id}";
  // static const REPLACE_ME = "/v2/messages/{id}";
  // static const REPLACE_ME = "/v2/messages/{id}/read";
  // static const REPLACE_ME = "/v2/notifications";
  // static const REPLACE_ME = "/v2/notifications/count";
  // static const REPLACE_ME = "/v2/notifications/intents";
  // static const REPLACE_ME = "/v2/notifications/{id}";
  // static const REPLACE_ME = "/v2/notifications/{id}/read";
  // static const REPLACE_ME = "/v2/schedules";
  // static const REPLACE_ME = "/v2/schedules/";
  // static const REPLACE_ME = "/v2/schedules/attendances";
  // static const REPLACE_ME = "/v2/schedules/subject_attendances";
  // static const REPLACE_ME = "/v2/schedules/{id}";
  // static const REPLACE_ME = "/v2/schedules/{id}";
  // static const REPLACE_ME = "/v2/schedules/{id}";
  // static const REPLACE_ME = "/v2/schedules/{id}/detail";
  // static const REPLACE_ME = "/v2/upload/{asset_type}";
  static const V2_USER = "/v2/user";
  // static const REPLACE_ME = "/v2/user/progress";
}

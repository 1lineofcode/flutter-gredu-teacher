import '../../base/networking/_index.dart';
import '../endpoint.dart';

///   created               : Aditya Pratama
///   originalFilename      : remote_repository
///   date                  : 12 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   scope                 : endpoint start with -> /user/*

class UserRepositories {
  var _service = ApiService();

  Future<Outcome> getUserProfile() async {
    return await _service.httpGet(
      endPoint: "/v2/user",
      withToken: true,
    );
  }
}

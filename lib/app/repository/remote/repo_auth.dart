import '../../base/networking/_index.dart';
import '../endpoint.dart';

///   created               : Aditya Pratama
///   originalFilename      : remote_repository
///   date                  : 12 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   scope                 : endpoint start with -> /authentication/*

class AuthRepositories {
  var _service = ApiService();

  Future<Outcome> postLogin(String email, String password) async {
    var loginBody = {'email': email, 'password': password};

    return await _service.httpPost(
      endPoint: Endpoint.LOGIN,
      withToken: false,
      body: loginBody,
    );
  }

}

class UserProfileResponse {
  Data? _data;
  int? _statusCode;

  Data? get data => _data;
  int? get statusCode => _statusCode;

  UserProfileResponse({
      Data? data, 
      int? statusCode}){
    _data = data;
    _statusCode = statusCode;
}

  UserProfileResponse.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
    _statusCode = json["status_code"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data?.toJson();
    }
    map["status_code"] = _statusCode;
    return map;
  }

}

class Data {
  User? _user;

  User? get user => _user;

  Data({
      User? user}){
    _user = user;
}

  Data.fromJson(dynamic json) {
    _user = json["user"] != null ? User.fromJson(json["user"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_user != null) {
      map["user"] = _user?.toJson();
    }
    return map;
  }

}

class User {
  String? _id;
  String? _fullName;
  String? _email;
  int? _pobId;
  String? _dob;
  String? _phoneNumber;
  String? _gender;
  String? _genderText;
  String? _religionId;
  Asset? _asset;
  Signature? _signature;

  String? get id => _id;
  String? get fullName => _fullName;
  String? get email => _email;
  int? get pobId => _pobId;
  String? get dob => _dob;
  String? get phoneNumber => _phoneNumber;
  String? get gender => _gender;
  String? get genderText => _genderText;
  String? get religionId => _religionId;
  Asset? get asset => _asset;
  Signature? get signature => _signature;

  User({
      String? id, 
      String? fullName, 
      String? email, 
      int? pobId, 
      String? dob, 
      String? phoneNumber, 
      String? gender, 
      String? genderText, 
      String? religionId, 
      Asset? asset, 
      Signature? signature}){
    _id = id;
    _fullName = fullName;
    _email = email;
    _pobId = pobId;
    _dob = dob;
    _phoneNumber = phoneNumber;
    _gender = gender;
    _genderText = genderText;
    _religionId = religionId;
    _asset = asset;
    _signature = signature;
}

  User.fromJson(dynamic json) {
    _id = json["id"];
    _fullName = json["full_name"];
    _email = json["email"];
    _pobId = json["pob_id"];
    _dob = json["dob"];
    _phoneNumber = json["phone_number"];
    _gender = json["gender"];
    _genderText = json["gender_text"];
    _religionId = json["religion_id"];
    _asset = json["asset"] != null ? Asset.fromJson(json["asset"]) : null;
    _signature = json["signature"] != null ? Signature.fromJson(json["signature"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["full_name"] = _fullName;
    map["email"] = _email;
    map["pob_id"] = _pobId;
    map["dob"] = _dob;
    map["phone_number"] = _phoneNumber;
    map["gender"] = _gender;
    map["gender_text"] = _genderText;
    map["religion_id"] = _religionId;
    if (_asset != null) {
      map["asset"] = _asset?.toJson();
    }
    if (_signature != null) {
      map["signature"] = _signature?.toJson();
    }
    return map;
  }

}

class Signature {
  dynamic? _id;
  dynamic? _filename;
  dynamic? _contentType;
  dynamic? _fileSize;
  dynamic? _docAwsUrl;

  dynamic? get id => _id;
  dynamic? get filename => _filename;
  dynamic? get contentType => _contentType;
  dynamic? get fileSize => _fileSize;
  dynamic? get docAwsUrl => _docAwsUrl;

  Signature({
      dynamic? id, 
      dynamic? filename, 
      dynamic? contentType, 
      dynamic? fileSize, 
      dynamic? docAwsUrl}){
    _id = id;
    _filename = filename;
    _contentType = contentType;
    _fileSize = fileSize;
    _docAwsUrl = docAwsUrl;
}

  Signature.fromJson(dynamic json) {
    _id = json["id"];
    _filename = json["filename"];
    _contentType = json["content_type"];
    _fileSize = json["file_size"];
    _docAwsUrl = json["doc_aws_url"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["filename"] = _filename;
    map["content_type"] = _contentType;
    map["file_size"] = _fileSize;
    map["doc_aws_url"] = _docAwsUrl;
    return map;
  }

}

class Asset {
  String? _id;
  String? _filename;
  String? _contentType;
  String? _fileSize;
  String? _docAwsUrl;

  String? get id => _id;
  String? get filename => _filename;
  String? get contentType => _contentType;
  String? get fileSize => _fileSize;
  String? get docAwsUrl => _docAwsUrl;

  Asset({
      String? id, 
      String? filename, 
      String? contentType, 
      String? fileSize, 
      String? docAwsUrl}){
    _id = id;
    _filename = filename;
    _contentType = contentType;
    _fileSize = fileSize;
    _docAwsUrl = docAwsUrl;
}

  Asset.fromJson(dynamic json) {
    _id = json["id"];
    _filename = json["filename"];
    _contentType = json["content_type"];
    _fileSize = json["file_size"];
    _docAwsUrl = json["doc_aws_url"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["filename"] = _filename;
    map["content_type"] = _contentType;
    map["file_size"] = _fileSize;
    map["doc_aws_url"] = _docAwsUrl;
    return map;
  }

}
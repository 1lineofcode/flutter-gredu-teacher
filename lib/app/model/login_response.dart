class LoginResponse {
  Data? _data;
  int? _statusCode;

  Data? get data => _data;
  int? get statusCode => _statusCode;

  LoginResponse({
      Data? data, 
      int? statusCode}){
    _data = data;
    _statusCode = statusCode;
}

  LoginResponse.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
    _statusCode = json["status_code"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data?.toJson();
    }
    map["status_code"] = _statusCode;
    return map;
  }

}

class Data {
  String? _authToken;
  User? _user;

  String? get authToken => _authToken;
  User? get user => _user;

  Data({
      String? authToken, 
      User? user}){
    _authToken = authToken;
    _user = user;
}

  Data.fromJson(dynamic json) {
    _authToken = json["auth_token"];
    _user = json["user"] != null ? User.fromJson(json["user"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["auth_token"] = _authToken;
    if (_user != null) {
      map["user"] = _user?.toJson();
    }
    return map;
  }

}

class User {
  String? _id;
  String? _email;

  String? get id => _id;
  String? get email => _email;

  User({
      String? id, 
      String? email}){
    _id = id;
    _email = email;
}

  User.fromJson(dynamic json) {
    _id = json["id"];
    _email = json["email"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["email"] = _email;
    return map;
  }

}
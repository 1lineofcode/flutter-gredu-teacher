import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

///   created               : Aditya Pratama
///   originalFilename      : log
///   date                  : 11 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///  Small, easy to use and extensible logger which prints beautiful logs.
///  Inspired by logger Android. (IntelliJ)

var _log = Logger(
  printer: PrettyPrinter(
      methodCount: 0, // number of method calls to be displayed
      errorMethodCount: 0, // number of method calls if stacktrace is provided
      lineLength: 120, // width of the output
      colors: true, // Colorful log messages
      printEmojis: true, // Print an emoji for each log message
      printTime: false, // Should each log print contain a timestamp
      stackTraceBeginIndex: 0,
  ),
);

var _tag = "GREDU LOG ->";

void log(message) {
  if (kDebugMode) _log.d("$_tag $message");
}

void print(message) {
  if (kDebugMode) _log.d("$_tag : $message");
}

void logD(message) {
  if (kDebugMode) _log.d("$_tag : $message");
}

void logE(message) {
  if (kDebugMode) _log.e("$_tag : $message");
}

void logI(message) {
  if (kDebugMode) _log.i("$_tag : $message");
}

void logW(message) {
  if (kDebugMode) _log.w("$_tag : $message");
}
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:velocity_x/velocity_x.dart';

class ExFormError extends StatelessWidget {
  final String errorMessage;

  const ExFormError({
    Key? key,
    required this.errorMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        VStack([
          errorMessage.isNotBlank
              ? Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(4),
                      child: SvgPicture.asset(
                        'assets/icons/fi-rr-exclamation.svg',
                        height: 20,
                        width: 20,
                        color: Vx.red500,
                      ),
                    ),
                    SizedBox(width: 8),
                    errorMessage.text.red500.make(),
                  ],
                )
              : SizedBox(height: 0)
        ]).scrollHorizontal(),
      ],
    );
  }
}

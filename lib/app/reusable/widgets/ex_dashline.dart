import 'package:flutter/cupertino.dart';
import 'package:gredu_teacher/app/base/resource/_index.dart';

import 'ex_progress.dart';

class ExDashLine extends StatelessWidget {
  const ExDashLine({
    Key? key,
    this.width,
    this.color,
  }) : super(key: key);

  final double? width;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Container(
            width: width ?? 30,
            height: 2,
            child: ExProgress(
              value: 100,
              color1: color ?? battleshipGrey,
              color2: color ?? battleshipGrey,
            ),
          ),
        ),
        SizedBox(height: 3),
        Center(
          child: Container(
            width: width ?? 30,
            height: 2,
            child: ExProgress(
              value: 100,
              color1: color ?? battleshipGrey,
              color2: color ?? battleshipGrey,
            ),
          ),
        ),
      ],
    );
  }
}

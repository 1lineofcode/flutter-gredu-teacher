import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class ExButtonOutline extends StatelessWidget {
  const ExButtonOutline({
    Key? key,
    this.width,
    this.height,
    required this.btnText,
    this.onPress,
    required this.color,
    this.radius,
    this.textSize,
  }) : super(key: key);

  final double? width;
  final double? height;
  final String btnText;
  final VoidCallback? onPress;
  final Color color;
  final double? radius;
  final double? textSize;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: OutlinedButton(
          onPressed: onPress,
          style: OutlinedButton.styleFrom(
              primary: color,
              side: BorderSide(color: color),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(radius ?? 8),
              )),
          child: btnText.text
              .textStyle(TextStyle(color: color))
              .size(textSize ?? 15)
              .make()),
    );
  }
}
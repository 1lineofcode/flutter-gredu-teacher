import 'package:get/get.dart';

import 'package:gredu_teacher/app/constants/_shared_preference_key.dart';
import 'package:gredu_teacher/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:gredu_teacher/app/modules/dashboard/views/dashboard_view.dart';

import '../modules/__test_page/bindings/test_page_binding.dart';
import '../modules/__test_page/views/test_page_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/view/login_view.dart';
import '../modules/onboarding/bindings/onboarding_binding.dart';
import '../modules/onboarding/views/onboarding_view.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.ROOT;

  static final routes = [
    // STARTER
    GetPage(
      name: Routes.ROOT,
      page: () =>
          pref.read(PREF_IS_LOGIN) == null ? OnBoardingView() : DashboardView(),
      binding: pref.read(PREF_IS_LOGIN) == null
          ? OnBoardingBinding()
          : DashboardBinding(),
    ),
    GetPage(
      name: Routes.TEST_PAGE,
      page: () => TestPageView(),
      binding: TestPageBinding(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),

    GetPage(
      name: Routes.ONBOARDING,
      page: () => OnBoardingView(),
      binding: OnBoardingBinding(),
    ),
    GetPage(
      name: Routes.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
    ),
  ];
}

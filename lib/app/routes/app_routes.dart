part of 'app_pages.dart';

/// [DO_NOT_EDIT]
/// this file generate by getx_cli
/// */

abstract class Routes {
  static const String ROOT = "/";
  static const String ONBOARDING = "/onboarding";
  static const String LOGIN = "/login";
  static const String REGISTER_1 = "/register_step_1";
  static const String REGISTER_2 = "/register_step_2";
  static const String FORGET = "/forget";
  static const String DASHBOARD = "/dashboard";
  static const TEST_PAGE = '/test-page';
}

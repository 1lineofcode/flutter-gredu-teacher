import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../constants/_shared_preference_key.dart';
import 'translation_lang_en.dart';
import 'translation_lang_id.dart';


class AppTranslations extends Translations {

  static final locale = Locale('id'); // Default locale
  static final fallbackLocale = Locale('id'); // dah lah indon aja ~

  static void init() {
    String? locale = pref.read(PREF_CURRENT_LANGUAGE) ?? "id";
    if(locale.isBlank!) {
      Get.updateLocale(Locale('id'));
      pref.write(PREF_CURRENT_LANGUAGE, 'id');
    } else {
      Get.updateLocale(Locale(locale));
    }
  }

  static void updateLocale({String langCode = 'id'}) {
    Get.updateLocale(Locale(langCode));
    pref.write(PREF_CURRENT_LANGUAGE, langCode);
  }

  @override
  Map<String, Map<String, String>> get keys => {
    'id': langID,
    'en': langEN,
  };
}
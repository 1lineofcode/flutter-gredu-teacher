///   created               : Aditya Pratama
///   originalFilename      : translation_lang_id
///   date                  : 11 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   mapping translation resource [indonesia]

final Map<String, String> langID = {
  'text_welcome_back': 'Selamat datang kembali 👋 ',
  'text_welcome_back_desc': 'Silahkan masuk dengan akun Anda yang terdaftar di aplikasi Gredu Teacher.',
  'text_signin': 'Masuk',
  'text_forget_password': 'Lupa Kata Sandi?',
  'text_copyright': 'Build with ❤️ by Flutter',
  'text_email_input': 'Masukkan alamat email',
  'text_password_input': 'Masukkan kata sandi',
};

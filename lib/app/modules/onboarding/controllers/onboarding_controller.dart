import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:gredu_teacher/app/base/lifecycle/_index.dart';

class OnBoardingController extends BaseController {

  final PageController myPageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  final _contentData = [].obs;
  get contentData => _contentData;
  set contentData(val) => _contentData;

  final _currentPage = 0.obs;
  get currentPage => _currentPage.value;
  set currentPage(val) => _currentPage.value = val;


  final _getListCounseling = [].obs;
  get getListCounseling => _getListCounseling;
  set getListCounseling(val) => _getListCounseling;


  @override
  Future<void> onInit()  async {
    super.onInit();
    fetchContentData();
    currentPage = 0;
  }

  @override
  void onClose() {
    currentPage = 0;
    super.onClose();
  }

  void fetchContentData() async {
    var list = [
      {
        "contentTitle": "Selamat datang di Gredu Teacher",
        "contentDescription": "Kami adalah asisten Anda.",
        "contentImage": "assets/images/onboarding_1.svg"
      },
      {
        "contentTitle": "Membuat dan Melihat Jadwal",
        "contentDescription": "Membantu Anda mengelola segala aktivitas.",
        "contentImage": "assets/images/onboarding_2.svg"
      },
      {
        "contentTitle": "Mengisi Daftar Hadir",
        "contentDescription": "Mengelola status dan tingkat kehadiran murid dengan lebih efisien.",
        "contentImage": "assets/images/onboarding_3.svg"
      },
      {
        "contentTitle": "Meninjau Sikap Murid",
        "contentDescription": "Buat laporan sikap murid setiap harinya secara lebih cepat.",
        "contentImage": "assets/images/onboarding_4.svg"
      },
      {
        "contentTitle": "Membuat dan Menerima Pesan",
        "contentDescription": "Lebih mudah bertukar kabar dengan wali murid.",
        "contentImage": "assets/images/onboarding_5.svg"
      },
    ];
    _contentData.assignAll(list);
  }
}

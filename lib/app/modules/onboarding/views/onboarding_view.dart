import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gredu_teacher/app/base/resource/_index.dart';
import 'package:gredu_teacher/app/modules/onboarding/views/widgets/content_item.dart';
import 'package:gredu_teacher/app/reusable/widgets/_index.dart';
import 'package:gredu_teacher/app/routes/app_pages.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/onboarding_controller.dart';

class OnBoardingView extends GetView<OnBoardingController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: VStack([
        Obx(() => Container(
              height: 300,
              width: double.infinity,
              child: controller.contentData.length != 0
                  ? buildPageView()
                  : Center(child: CircularProgressIndicator()),
            )),
        Obx(() => Row(
              children: List.generate(
                controller.contentData.length,
                (index) => buildDot(index),
              ),
            )),
        SizedBox(height: 16),
        Obx(() => buildTextContent(controller.currentPage ?? 0)),
        Obx(
          () => controller.currentPage == 4
              ? buildColumnLastPage()
              : buildRowCurrentPage().marginOnly(top: 24),
        ),
      ]).p24().scrollVertical(),
    );
  }


  // —————————————————————————————————————————————————————————————————————————
  // SPLIT COMPONENT —————————————————————————————————————————————————————————
  // —————————————————————————————————————————————————————————————————————————
  PageView buildPageView() {
    return PageView.builder(
      itemCount: controller.contentData.length,
      itemBuilder: (context, index) {
        return Obx(
          () => OnBoardContent(
            contentImage: controller.contentData[index]["contentImage"],
          ),
        );
      },
      controller: controller.myPageController,
      onPageChanged: (value) {
        controller.currentPage = value;
      },
    );
  }

  AnimatedContainer buildDot(int index) {
    return AnimatedContainer(
      margin: EdgeInsets.only(right: 8),
      height: 10,
      width: controller.currentPage == index ? 32 : 10,
      decoration: BoxDecoration(
          color: controller.currentPage == index ? colorPrimary : Vx.gray400,
          borderRadius: BorderRadius.circular(10)),
      duration: Duration(milliseconds: 100),
    );
  }

  Row buildRowCurrentPage() {
    return Row(
      children: [
        Spacer(),
        RawMaterialButton(
          elevation: 1,
          fillColor: colorPrimary,
          child: Icon(Icons.arrow_forward_rounded),
          shape: CircleBorder(),
          padding: EdgeInsets.all(15.0),
          onPressed: () {
            controller.currentPage += 1;
            controller.myPageController.animateToPage(
              controller.currentPage,
              curve: Curves.easeIn,
              duration: Duration(milliseconds: 100),
            );
          },
        ),
      ],
    );
  }

  Column buildColumnLastPage() {
    return Column(
      children: [
        HStack(
          [
            buildButtonRegister(),
            SizedBox(width: 8),
            buildButtonLogin(),
          ],
        ).marginOnly(top: 24),
      ],
    );
  }

  AnimatedContainer buildTextContent(int? index) {
    String title = controller.contentData[index]["contentTitle"];
    String description = controller.contentData[index]["contentDescription"];
    return AnimatedContainer(
      width: double.infinity,
      height: 70,
      child: VStack([
        title.text.size(18).bold.make(),
        description.text.color(battleshipGrey).make().marginOnly(top: 8),
      ]),
      duration: Duration(milliseconds: 500),
    );
  }

  Flexible buildButtonLogin() {
    return Flexible(
      flex: 1,
      fit: FlexFit.tight,
      child: ExButtonDefault(
        height: 50,
        btnText: "Masuk",
        bgColor: colorPrimary,
        textColor: colorWhite,
        onPress: () {
          Get.toNamed(Routes.LOGIN);
        },
      ),
    );
  }

  Flexible buildButtonRegister() {
    return Flexible(
      flex: 1,
      fit: FlexFit.tight,
      child: ExButtonOutline(
        height: 50,
        btnText: "Daftar",
        color: colorPrimary,
        radius: 8,
        textSize: 15,
        onPress: () {
          Get.toNamed(Routes.REGISTER_1);
        },
      ),
    );
  }
}

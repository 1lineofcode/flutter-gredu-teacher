import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class OnBoardContent extends GetView {
  const OnBoardContent({
    Key? key,
    required this.contentImage,
  }) : super(key: key);
  final String contentImage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SvgPicture.asset(contentImage,
            placeholderBuilder: (BuildContext context) => Container(
                  padding: const EdgeInsets.all(45),
                  child: const CircularProgressIndicator(),
                )),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../../base/resource/_index.dart';
import '../../../reusable/utils/_index.dart';
import '../../../reusable/widgets/_index.dart';
import '../controller/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorPrimary,
        iconTheme: IconThemeData(color: colorWhite),
      ),
      backgroundColor: colorPrimary,
      body: VStack([
        SizedBox(height: context.percentHeight * 5),
        //Center(child: SvgPicture.asset("assets/images/illustration_login.svg")),
        Container(
          width: context.mq.size.width,
          height: context.mq.size.height,
          decoration: buildBoxDecoration(),
          child: VStack([
            Center(child: buildHeaderVStack()).marginOnly(top: 16),
            SizedBox(height: 35),
            buildVxTextFieldEmail(context),
            SizedBox(height: 20),
            buildVxTextFieldPassword(context),
            SizedBox(height: 16),
            buildErrorForm(),
            SizedBox(height: 16),
            buildForgetPasswordButton(),
            SizedBox(height: 20),
            buildLoginButton()
          ]).p24(),
        ),
      ]).scrollVertical(),
    );
  }

  // —————————————————————————————————————————————————————————————————————————
  // SPLIT COMPONENT —————————————————————————————————————————————————————————
  // —————————————————————————————————————————————————————————————————————————
  VStack buildHeaderVStack() {
    return VStack(
      [
        SvgPicture.asset("assets/images/gredu_logo.svg"),
        "Masuk".text.size(29).bold.make().marginOnly(top: 16),
        "Isi data untuk mulai menggunakan Gredu."
            .text
            .color(battleshipGrey)
            .make()
            .marginOnly(top: 8),
      ],
      alignment: MainAxisAlignment.center,
      crossAlignment: CrossAxisAlignment.center,
    );
  }

  BoxDecoration buildBoxDecoration() {
    return BoxDecoration(
      color: colorWhite,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(40),
        topRight: Radius.circular(40),
      ),
    );
  }

  Center buildForgetPasswordButton() {
    return Center(
      child: HStack(
        [
          "Lupa kata sandi ".text.make(),
          "Klik Di sini"
              .text
              .center
              .underline
              .color(colorPrimary)
              .size(15)
              .make()
              .onTap(() => controller.goToForgetPasswordPage())
        ],
      ),
    );
  }

  GetBuilder buildLoginButton() {
    return GetBuilder<LoginController>(
      init: LoginController(),
      initState: (_) {},
      builder: (_) {
        return ExButtonDefault(
          height: 50,
          width: double.infinity,
          bgColor: colorPrimary,
          textColor: colorWhite,
          btnText: "text_signin".tr,
          onPress: () {
            var email = _.tfEmailController.text;
            var pass = _.tfPasswordController.text;
            controller.doLogin(email, pass);
          },
        );
      },
    );
  }

  GetBuilder buildErrorForm() {
    return GetBuilder<LoginController>(
      init: LoginController(),
      initState: (_) {},
      builder: (_) {
        return ExFormError(errorMessage: _.error.value);
      },
    );
  }

  GetBuilder buildVxTextFieldEmail(BuildContext context) {
    return GetBuilder<LoginController>(
      init: LoginController(),
      initState: (_) {},
      builder: (_) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(color: containerGrey),
            borderRadius: BorderRadius.circular(8),
          ),
          child: VStack([
            "Email".text.size(13).make(),
            VxTextField(
              borderType: VxTextFieldBorderType.none,
              fillColor: colorWhite,
              hint: "text_email_input".tr,
              value: 'adit@gmail.com',
              focusNode: _.tfEmailFocus,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(color: colorBlack),
              textInputAction: TextInputAction.next,
              controller: _.tfEmailController,
              onChanged: (value) => _.onEmailChange(value),
              onEditingComplete: () => fieldFocusChange(
                context,
                _.tfEmailFocus,
                _.tfPasswordFocus,
              ),
            )
          ]).paddingOnly(top: 16, left: 16, bottom: 8, right: 16),
        );
      },
    );
  }

  GetBuilder buildVxTextFieldPassword(BuildContext context) {
    return GetBuilder<LoginController>(
      init: LoginController(),
      initState: (_) {},
      builder: (_) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(color: containerGrey),
            borderRadius: BorderRadius.circular(8),
          ),
          child: VStack([
            "Password".text.size(13).make(),
            VxTextField(
              borderType: VxTextFieldBorderType.none,
              fillColor: colorWhite,
              isPassword: true,
              obscureText: true,
              hint: "text_password_input".tr,
              value: 'gredu123',
              style: TextStyle(color: Vx.gray400),
              textInputAction: TextInputAction.done,
              focusNode: _.tfPasswordFocus,
              controller: _.tfPasswordController,
              onChanged: (value) => _.onPasswordChange(value),
              onEditingComplete: () {
                FocusScope.of(context).unfocus();
                _.doLogin(
                    _.tfEmailController.text, _.tfPasswordController.text);
              },
            ),
          ]).paddingOnly(top: 16, left: 16, bottom: 8, right: 16),
        );
      },
    );
  }
}

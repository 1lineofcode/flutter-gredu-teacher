import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gredu_teacher/app/model/user_profile_response.dart';
import 'package:gredu_teacher/app/routes/app_pages.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../../base/lifecycle/base_controller.dart';
import '../../../constants/_common.dart';
import '../../../constants/_shared_preference_key.dart';
import '../../../model/login_response.dart';

class LoginController extends BaseController {
  final tfEmailController = TextEditingController();
  final tfPasswordController = TextEditingController();
  final tfEmailFocus = FocusNode();
  final tfPasswordFocus = FocusNode();
  
  final error = ''.obs;


  @override
  void onInit() {
    super.onInit();
    if(kDebugMode) {
      tfEmailController.text = "erna.nur@yopmail.com";
      tfPasswordController.text = "gredu123";
    }
  }

  void doLogin(String email, String password) async {

    if (email.isEmptyOrNull || password.isEmptyOrNull) {
      showSnackBar(
        title: 'Field Require',
        message: 'field email & password harus diisi',
      );
    } else {
      showLoadingDialog();
      var apiResponse = await repoAuth.postLogin(email, password);
      hideLoadingDialog();
      error.value = BLANK;

      if (apiResponse.hashError) {
        error.value = apiResponse.errorMessages ?? "";
      } else {

        // STEP : LOGIN SUKSES
        var response1 = LoginResponse.fromJson(apiResponse.body);
        pref.write(PREF_IS_LOGIN, true);
        pref.write(PREF_TOKEN, response1.data?.authToken ?? "");

        // STEP : GETTING DATA CURRENT USER PROFILE
        var response2 = await getDataUser();
        if (response2 != null) {
          await pref.write(PREF_CURRENT_USER_PROFILE, response2);

          // TODO : REGISTER FIREBASE TOKEN
          // code here ...

          // STEP : GO DASHBOARD
          Get.offAndToNamed(Routes.DASHBOARD);
          var userFullName = response2.data?.user?.fullName ?? "";
          showSuccessSnackBar(title: "Login Sukses", message: "hi $userFullName");
        }
      }
    }
    update();
  }

  Future<UserProfileResponse?> getDataUser() async {
    showLoadingDialog();
    var apiResponse = await repoUser.getUserProfile();
    hideLoadingDialog();
    if (apiResponse.hashError) {
      error.value = apiResponse.errorMessages ?? "";
      return null;
    } else {
      return UserProfileResponse.fromJson(apiResponse.body);
    }
  }

  void onEmailChange(String value) {
    if (value.isNotBlank && !value.validateEmail()) {
      error.value = "Email tidak valid";
    } else {
      error.value = BLANK;
    }
    update();
  }

  void onPasswordChange(String value) {
    if (value.isNotBlank && value.length <= 5) {
      error.value = "Password harus lebih besar dari 6 huruf";
    } else {
      error.value = BLANK;
    }
    update();
  }

  void goToForgetPasswordPage() => Get.toNamed(Routes.FORGET);

}

import 'package:get/get.dart';
import 'package:gredu_teacher/app/base/lifecycle/_index.dart';
import 'package:gredu_teacher/app/constants/_shared_preference_key.dart';
import 'package:gredu_teacher/app/model/user_profile_response.dart';
import 'package:gredu_teacher/app/routes/app_pages.dart';

class DashboardController extends BaseController with StateMixin<User> {

  final currentUserName = ''.obs;
  final errorData = ''.obs;

  @override
  Future<void> onInit() async {
    getDataUser();
    super.onInit();
  }

  Future<void> getDataUser() async {
    try {
      var currentUserPref = await pref.read(PREF_CURRENT_USER_PROFILE);
      var cu = UserProfileResponse.fromJson(currentUserPref);
      currentUserName.value = cu.data?.user?.fullName ?? "";
      change(cu.data?.user, status: RxStatus.success());
    } catch(e) {
      errorData.value = "$e";
      change(null, status: RxStatus.error());
    }
  }

  void logout() {
    pref.erase();
    Get.offAndToNamed(Routes.ONBOARDING);
  }
}

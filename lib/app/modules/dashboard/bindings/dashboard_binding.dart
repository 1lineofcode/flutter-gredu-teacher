import 'package:get/get.dart';
import 'package:gredu_teacher/app/modules/login/controller/login_controller.dart';

import '../controllers/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<LoginController>(() => LoginController());
  }
}

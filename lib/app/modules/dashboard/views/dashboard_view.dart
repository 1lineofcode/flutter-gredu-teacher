import 'package:avatars/avatars.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:gredu_teacher/app/base/resource/_index.dart';
import 'package:gredu_teacher/app/base/ui_state/ui_error.dart';
import 'package:gredu_teacher/app/base/ui_state/ui_loading.dart';
import 'package:gredu_teacher/app/reusable/widgets/_index.dart';
import 'package:gredu_teacher/app/reusable/widgets/ex_dashline.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorPrimary,
      body: controller.obx(
        (state) => buildUIOk(context).scrollVertical(),
        onLoading: ExUiLoading(),
        onError: (error) => ExUiError(
          errorMessage: controller.errorData.value,
          onRetry: () => controller.getDataUser(),
        ),
      ),
      bottomNavigationBar: buildBottomNavigationBar(),
    );
  }

  VStack buildUIOk(BuildContext context) {
    return VStack([
      SizedBox(height: context.percentHeight * 5),
      Obx(() => buildCardUserProfile(context)),
      Container(
        width: context.mq.size.width,
        height: context.percentHeight * 90,
        decoration: buildBoxDecoration(),
        child: VStack([
          ExDashLine(color: paleGrey),
          SizedBox(height: 28),
          buildCardProfileCompleteness(context),
          SizedBox(height: 16),
          buildCardNotification(context).cornerRadius(8),
          SizedBox(height: 24),
          HStack([
            buildMenu("menu_home_1.svg", "Absensi"),
            buildMenu("menu_home_2.svg", "Pembelajaran"),
            buildMenu("menu_home_3.svg", "Penilaian"),
          ]).p(4),
          SizedBox(height: 16),
          HStack([
            buildMenu("menu_home_1.svg", "Daftar Nilai"),
            buildMenu("menu_home_1.svg", "Konseling"),
          ]),
          ExButtonDefault(
              btnText: "Logout", onPress: () => controller.logout()),
        ]).marginOnly(left: 24, right: 24, top: 16, bottom: 16),
      )
    ]);
  }

  // —————————————————————————————————————————————————————————————————————————
  // SPLIT COMPONENT —————————————————————————————————————————————————————————
  // —————————————————————————————————————————————————————————————————————————
  Avatar buildAvatar() {
    return Avatar(name: 'Anderson', sources: [
      NetworkSource('https://static.asiachan.com/Lisa.full.87638.jpg')
    ], placeholderColors: [
      battleshipGrey,
      battleshipGrey,
    ]);
  }

  BoxDecoration buildBoxDecoration() {
    return BoxDecoration(
      color: colorWhite,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    );
  }

  Container buildCardProfileCompleteness(BuildContext context) {
    return Container(
      child: VStack([
        "Kelengkapan Profil".text.bold.make(),
        HStack([
          Container(
            width: context.percentWidth * 70,
            child: ExProgress(value: 50),
          ),
          Spacer(),
          "50%".text.bold.make(),
        ]).marginOnly(top: 8),
        "Ayo lengkapi profil Anda!".text.make().marginOnly(top: 8),
      ]),
    );
  }

  Container buildCardUserProfile(BuildContext context) {
    var zz = controller.currentUserName;
    return Container(
      height: 128,
      width: context.mq.size.width,
      color: colorPrimary,
      child: HStack([
        Container(
          width: 70,
          height: 70,
          child: buildAvatar(),
        ),
        VStack([
          "Halo, $zz".text.white.make(),
          "SMA Diponegoro ".text.white.bold.size(16).make(),
        ]).marginOnly(left: 16),
      ]).paddingOnly(left: 24, right: 24),
    );
  }

  Container buildCardNotification(BuildContext context) {
    return Container(
      height: 70,
      width: context.mq.size.width,
      color: Vx.orange300,
      child: VStack([
        HStack(
          [
            "Upcoming Event".text.bold.orange900.make(),
            Spacer(),
            SvgPicture.asset(
              'assets/icons/fi-rr-cross-small.svg',
              height: 20,
              width: 20,
              color: Vx.orange900,
            ),
          ],
        ),
        "Class Meeting (20-24 Jan 2020)".text.orange900.make(),
      ]).paddingAll(16),
    );
  }

  Column buildMenu(String? icon, String label) {
    return Column(
      children: [
        Container(
          height: 60,
          width: 100,
          child: VxBox(
            child: SvgPicture.asset(
              'assets/images/$icon',
            ).p(16),
          )
              .square(100)
              .roundedFull
              .neumorphic(
                color: colorWhite,
                elevation: 10,
                curve: VxCurve.emboss,
              )
              .make(),
        ),
        SizedBox(height: 16),
        "$label".text.size(12).make()
      ],
    );
  }

  BottomNavigationBar buildBottomNavigationBar() {
    return BottomNavigationBar(
      selectedItemColor: colorPrimary,
      unselectedItemColor: Vx.gray500,
      elevation: 10,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      iconSize: 20,
      selectedFontSize: 13,
      unselectedFontSize: 13,
      type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
          icon: Icon(ExIcons.fi_rr_home),
          label: 'Performa',
        ),
        BottomNavigationBarItem(
          icon: Icon(ExIcons.fi_rr_calendar),
          label: 'Jadwal',
        ),
        BottomNavigationBarItem(
          icon: Icon(ExIcons.fi_rr_envelope),
          label: 'Pembayaran',
        ),
        BottomNavigationBarItem(
          icon: Icon(ExIcons.fi_rr_sign_out),
          label: 'Profile',
        ),
      ],
    );
  }
}

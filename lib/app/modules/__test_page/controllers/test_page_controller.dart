import 'package:get/get.dart';
import 'package:gredu_teacher/app/base/lifecycle/_index.dart';
import 'package:gredu_teacher/app/constants/_common.dart';
import 'package:gredu_teacher/app/constants/_shared_preference_key.dart';
import 'package:gredu_teacher/app/model/login_response.dart';

///   created               : Aditya Pratama
///   originalFilename      : test_page_controller
///   date                  : 14 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///

class TestPageController extends BaseController {

  final output = ''.obs;
  final count = 0.obs;

  void increment() => count.value++;

  void reset() {
    count.value = 0;
    output.value = EMPTY;
    pref.erase();
  }

  void testHttpGet() async {
    showLoadingDialog();
    var apiResponse = await repoUser.getUserProfile();
    hideLoadingDialog();
    if (apiResponse.hashError)
      output.value = "Login dulu cuy~, tokennya belum ada -,-"; // custom error message
    else
      output.value = apiResponse.body.toString();
  }

  void testHttpPost() async {
    var email = 'erna.nur@yopmail.com';
    var password = 'gredu123';
    showLoadingDialog();
    var apiResponse = await repoAuth.postLogin(email, password);
    hideLoadingDialog();
    if (apiResponse.hashError)
      output.value = apiResponse.errorMessages ?? ""; // error message from api
    else {
      output.value = apiResponse.body.toString();
      var response = LoginResponse.fromJson(apiResponse.body); // parsing to model
      pref.write(PREF_TOKEN, response.data?.authToken);
    }
  }
}

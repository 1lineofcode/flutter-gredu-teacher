import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gredu_teacher/app/reusable/widgets/ex_star_rating.dart';
import 'package:gredu_teacher/app/routes/app_pages.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../../../app/reusable/widgets/_index.dart';
import '../../../base/resource/_index.dart';
import '../controllers/test_page_controller.dart';

class TestPageView extends GetView<TestPageController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: "TESTING PAGE".text.bold.make()),
      body: VStack([
        Row(
          children: [
            "HTTP TESTER".text.make(),
            Spacer(),
            ExStarRating(starCount: 5, rating: 2.3, color: colorPrimary, size: 24),
          ],
        ),
        ExProgress(value: 10),
        Divider(),
        ExButtonOutline(
          btnText: 'HTTP GET',
          width: double.infinity,
          color: colorPrimary,
          onPress: () => controller.testHttpGet(),
        ),
        ExButtonOutline(
          btnText: 'HTTP POST',
          width: double.infinity,
          color: colorPrimary,
          onPress: () => controller.testHttpPost(),
        ),
        ExButtonOutline(
          btnText: 'HTTP PUT',
          width: double.infinity,
          color: colorPrimary,
          onPress: () {},
        ),
        ExButtonOutline(
          btnText: 'HTTP DELETE',
          width: double.infinity,
          color: Vx.red500,
          onPress: () {},
        ),
        ExButtonOutline(
          btnText: 'HTTP UPLOAD',
          width: double.infinity,
          color: Vx.black,
          onPress: () {},
        ),
        ExButtonOutline(
          btnText: 'HTTP DOWNLOAD',
          width: double.infinity,
          color: colorPrimary,
          onPress: () {},
        ),
        Divider(),
        ExButtonDefault(
          btnText: 'GO LOGIN PAGE',
          bgColor: colorPrimary,
          textColor: colorWhite,
          width: double.infinity,
          onPress: () {
            Get.toNamed(Routes.LOGIN);
          },
        ),
        Divider(),
        Row(
          children: [
            ExButtonDefault(
              btnText: "Incement Value ++",
              bgColor: colorAccent,
              textColor: colorWhite,
              onPress: () => controller.increment(),
            ).marginOnly(right: 8),
            ExButtonDefault(
              btnText: "Reset",
              bgColor: Vx.red500,
              textColor: colorWhite,
              onPress: () => controller.reset(),
            ),
            Spacer(),
            Obx(() => controller.count.value.text.make()),
          ],
        ),
        Divider(),
        "RESPONSE FROM API".text.make(),
        Obx(() => controller.output.value.text.make()),
      ]).p24().scrollVertical(),
    );
  }
}

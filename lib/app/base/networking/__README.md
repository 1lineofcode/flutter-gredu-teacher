## TODO 
[x] support standard http (GET, POST, PUT, DELETE)
[x] support upload / download file multipart http
[x] Intercept requests / response
[x] support concurrent / multiple request
    -> bisa request lebih dari 1 api dalam 1 event 
[x] centralize result resource from api request
    -> untuk handle scenario response API nya bukan bentuk json -_- 
[x] customize / modify error handling
[x] Automatic reconnection in case of expired token
[x] Cache driver for easy connection with GetStorage. With the creation of the client, 
    you must create an isolate, so that the json coding occurs in the background, to prevent janks
[x] Response.data must return a dynamic or a Map, not a String, and ready for use, 
    without the need to use jsonDecode.
[x] Possibility to define token, and directly pass a string function that retrieves it.
[x] Ability to define the base url.
[x] Ability to Run on android, ios, web, linux, mac and windows.
[x] Ability to enable and disable secure traffic.



// —————————————————————————————————————————————————————————————————————————————
## Use example:

#### GET
```dart
var _service = ApiService();
Future<Outcome> getCounseling(String search) async {
    

return await _service.httpGet(
      endPoint: "/v2/counselings",
      withToken: true,
      query: { 'search': search },
    );


}

// OR 1 line example
Future<Outcome> getUser(String id) async => await _service.httpGet('/user/${id}');

``` 

#### POST
```dart
var _service = ApiService();
Future<Outcome> postLogin(String email, String password) async {
    var body = {'email': email, 'password': password};
    return await _service.httpPost(
      endPoint: "/authentication/request_token",
      withToken: false,
      body: body,
    );
}
``` 
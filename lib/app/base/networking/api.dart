import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:gredu_teacher/app/constants/_common.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../constants/_shared_preference_key.dart';
import '../../reusable/utils/_index.dart';
import 'outcome.dart';

///   create                : Aditya Pratama
///   originalFilename      : api
///   date                  : 11 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   yep, i know [dio] / [http] which work very well, don't get me wrong
///   but.. this the reason why i use GetConnect :
///   - big reason is .. keep on ecosystem
///   - https://github.com/jonataslaw/getx/issues/850#issuecomment-738124446
///   - https://github.com/jonataslaw/getx/issues/797#issue-743037822
///
///   and oh hey ! please read [__README.md]
/// ———————————————————————————————————————————————————————————————————————————————

final baseURL = dotenv.env['BASE_URL'];
enum Method {GET, POST, PUT, DELETE}

@visibleForTesting
class ApiService {
  var _api = _Api();

  Future<Outcome> httpGet({
    String endPoint = "",
    Map<String, String>? query,
    bool withToken = true,
  }) async {
    return await _api.httpGet(endPoint: endPoint, query: query, withToken: withToken);
  }

  Future<Outcome> httpPost({
    String endPoint = "",
    Map? body,
    bool withToken = true,
  }) async {
    return await _api.httpPost(endPoint: endPoint, body: body, withToken: withToken);
  }

  Future<Outcome> httpPut({
    String endPoint = "",
    Map? body,
    bool withToken = true,
  }) async {
    return await _api.httpPut(endPoint: endPoint, body: body, withToken: withToken);
  }

  Future<Outcome> httpDelete({
    String endPoint = "",
    String? id,
    bool withToken = true,
  }) async {
    return await _api.httpDelete(endPoint: "$endPoint/$id", withToken: withToken);
  }

  Future<Outcome> httpUploadMultipart({
    String endPoint = "",
    required File file,
    required String filename,
    bool withToken = true,
  }) async {
    return await _api.httpUploadMultipart(endPoint: endPoint, file: file, fileName: filename, withToken: withToken);
  }

  Future<Outcome> httpManual({
    required Method method,
    required String url,
    Map<String, String>? header,
    Map<String, String>? query,
    Map? body,
  }) async {
    return await _api.httpManual(
      method: method,
      url: url,
      header: header,
      query: query,
      body: body,
    );
  }
}

// —————————————————————————————————————————————————————————————————————————————
/// PRIVATE CLASS
/// configure http here !!
// —————————————————————————————————————————————————————————————————————————————
class _Api extends GetConnect {
  var _result = Outcome();
  bool _withToken = false;

  @override
  void onInit() {
    prepareHeader();
    super.onInit();
  }

  Future<void> prepareHeader() async {
    httpClient.errorSafety = true;
    httpClient.baseUrl = baseURL;

    // HEADER
    String deviceId = await DeviceInfo().uuid();
    String deviceType = Platform.operatingSystem;
    String appId = "ta";

    httpClient.addRequestModifier<void>((request) async {
      if (_withToken) {
        String? token = pref.read(PREF_TOKEN) ?? EMPTY;
        if (token.isNotBlank) {
          request.headers['Authorization'] = "Bearer $token";
        }
      }
      request.headers['Device-ID'] = "$deviceId";
      request.headers['Device-Type'] = "$deviceType";
      request.headers['App-ID'] = "$appId";
      request.headers['Referer'] = "https://mobile.gredu.co/*";

      // log("HEADERS ${request.headers}");

      return request;
    });
  }


  Future<Outcome> httpGet({
    required String endPoint,
    Map<String, String>? query,
    bool withToken = false,
  }) async {
    _withToken = withToken;
    await prepareHeader();

    log("GET     : ${httpClient.baseUrl}$endPoint");
    log("QUERY   : $query");
    if (withToken) log("TOKEN   : ${pref.read(PREF_TOKEN)}");

    try {
      var res = await get(endPoint, query: query);
      log("RESPONSE CODE : ${res.statusCode}");

      if (res.isOk) {
        log("${res.bodyString}");
        _result.body = res.body;
        _result.hashError = false;
        return _result;
      } else {
        return errorInterceptorHandling(res, _result);
      }
    } catch (e) {
      return errorInterceptorHandling(e, _result);
    }
  }


  Future<Outcome> httpManual({
    required Method method,
    required String url,
    Map<String, String>? header,
    Map<String, String>? query,
    Map? body,
  }) async {
    httpClient.baseUrl = EMPTY;
    log("GET     : $url");
    try {
      var res;
      switch(method) {
        case Method.GET:
          res = await get(url, query: query, headers: header);
          break;
        case Method.POST:
          res = await post(url, body, headers: header);
          break;
        case Method.PUT:
          res = await put(url, body, headers: header);
          break;
        case Method.DELETE:
          res = await delete(url, headers: header);
          break;
        default:
          res = await get(url, query: query, headers: header);
          break;
      }
      log("RESPONSE CODE : $res");
      if (res.isOk) {
        log("${res.bodyString}");
        _result.body = res.body;
        _result.hashError = false;
        return _result;
      } else {
        return errorInterceptorHandling(res, _result);
      }
    } catch (e) {
      return errorInterceptorHandling(e, _result);
    }
  }

  // —————————————————————————————————————————————————————————————————————————————
  // [POST]
  // —————————————————————————————————————————————————————————————————————————————
  Future<Outcome> httpPost({
    String endPoint = "",
    Map? body,
    bool withToken = true,
  }) async {
    _withToken = withToken;
    await prepareHeader();

    log("POST    : ${httpClient.baseUrl}$endPoint");
    log("PAYLOAD : $body");
    if (withToken) log("TOKEN   : ${pref.read(PREF_TOKEN)}");

    try {
      var res = await httpClient.post(endPoint, body: body);
      log("RESPONSE CODE : ${res.statusCode}");

      if (res.isOk) {
        log("${res.bodyString}");
        _result.body = res.body;
        _result.hashError = false;
        return _result;
      } else {
        return errorInterceptorHandling(res, _result);
      }
    } catch (e) {
      return errorInterceptorHandling(e, _result);
    }
  }

  // —————————————————————————————————————————————————————————————————————————————
  // [PUT]
  // —————————————————————————————————————————————————————————————————————————————
  Future<Outcome> httpPut({
    String endPoint = "",
    Map? body,
    bool withToken = true,
  }) async {
    _withToken = withToken;
    await prepareHeader();

    log("PUT     : ${httpClient.baseUrl}$endPoint");
    log("PAYLOAD : $body");
    if (withToken) log("TOKEN   : ${pref.read(PREF_TOKEN)}");

    try {
      var res = await httpClient.put(endPoint, body: body);
      log("RESPONSE CODE : ${res.statusCode}");

      if (res.isOk) {
        log("${res.bodyString}");
        _result.body = res.body;
        _result.hashError = false;
        return _result;
      } else {
        return errorInterceptorHandling(res, _result);
      }
    } catch (e) {
      return errorInterceptorHandling(e, _result);
    }
  }

  // —————————————————————————————————————————————————————————————————————————————
  // [DELETE]
  // —————————————————————————————————————————————————————————————————————————————
  Future<Outcome> httpDelete({
    String endPoint = "",
    bool withToken = true,
  }) async {
    _withToken = withToken;
    await prepareHeader();

    log("DELETE  : ${httpClient.baseUrl}$endPoint");
    if (withToken) log("TOKEN   : ${pref.read(PREF_TOKEN)}");

    try {
      var res = await httpClient.delete("$endPoint");
      log("RESPONSE CODE : ${res.statusCode}");

      if (res.isOk) {
        log("${res.bodyString}");
        _result.body = res.body;
        _result.hashError = false;
        return _result;
      } else {
        return errorInterceptorHandling(res, _result);
      }
    } catch (e) {
      return errorInterceptorHandling(e, _result);
    }
  }

  Future<Outcome> httpUploadMultipart({
    String endPoint = "",
    required File file,
    required String fileName,
    bool withToken = true,
  }) async {
    _withToken = withToken;
    await prepareHeader();

    log("UPLOAD  : ${httpClient.baseUrl}$endPoint");
    log("FILE    : $file");
    if (withToken) log("TOKEN   : ${pref.read(PREF_TOKEN)}");

    try {
      final form = FormData({'file': MultipartFile(file, filename: fileName)});
      var res = await httpClient.post(endPoint, body: form);
      log("RESPONSE CODE : ${res.statusCode}");

      if (res.isOk) {
        log("${res.bodyString}");
        _result.body = res.body;
        _result.hashError = false;
        return _result;
      } else {
        return errorInterceptorHandling(res, _result);
      }
    } catch (e) {
      return errorInterceptorHandling(e, _result);
    }
  }

  // —————————————————————————————————————————————————————————————————————————————
  // error handling
  // —————————————————————————————————————————————————————————————————————————————
  Outcome errorInterceptorHandling(res, Outcome result) {
    log("errorHandling : $res");
    try {
      switch (res.statusCode) {
        case 400:
          result.hashError = true;
          result.errorMessages = res.bodyString;
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
        case 401:
          result.hashError = true;
          result.errorMessages = "Email atau Password Salah";
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
        case 404:
          result.hashError = true;
          result.errorMessages = "Path tidak ditemukan (404)";
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
        case 405:
          result.hashError = true;
          result.errorMessages = "Method not allowed (405)";
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
        case 500:
          result.hashError = true;
          result.errorMessages = "Internal Server Error (500)";
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
        case 503:
          result.hashError = true;
          result.errorMessages = "Internal Server Error (503)";
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
        default: // offline
          result.hashError = true;
          result.errorMessages = "Tidak dapat terhubung ke server.";
          log("ERROR ${res.statusCode} : ${res.bodyString}");
          return result;
      }
    } catch (e) {
      result.hashError = true;
      result.errorMessages = "$e";
      log("ERROR Exception : $e");
      return result;
    }
  }
}

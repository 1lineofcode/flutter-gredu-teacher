import 'dart:convert';

import 'package:flutter/cupertino.dart';

///   created               : Aditya Pratama
///   originalFilename      : result
///   date                  : 11 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   centralize outcome response from api request .
///   biar apa? in case response api nya bukan bentuk json atau error aneh2, masih bisa dihandle ~
///

@visibleForTesting
class Outcome<T> {
  Outcome({
    this.hashError = false,
    this.errorMessages,
  });

  bool hashError;
  String? statusCode;
  String? errorMessages;
  T? body;

  factory Outcome.fromJson(String str) => Outcome.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Outcome.fromMap(Map<String, dynamic> json) => Outcome(
      errorMessages: json["error_messages"] == null ? "Tidak ada koneksi internet" : json["error_messages"]);

  Map<String, dynamic> toMap() => {
        "error_messages": errorMessages,
      };
}

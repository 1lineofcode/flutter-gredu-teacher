///   created               : Aditya Pratama
///   originalFilename      : api_exception
///   date                  : 12 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   Custom Exception Class ~,~
class GreduException implements Exception {
  String _message = "";

  // default value ~
  GreduException([String message = 'Ada yang kurang nich ~']) {
    this._message = message;
  }

  @override
  String toString() {
    return _message;
  }
}

/** use example

    void validateScore(int value) {
      if (value == null) {
        throw new GreduException();
      } else if (value <= 0) {
        throw new GreduException("Value must be greater than 0");
      } else if (value > 100) {
        throw new GreduException("Value must not be greater than 100");
      }
    }
 * */
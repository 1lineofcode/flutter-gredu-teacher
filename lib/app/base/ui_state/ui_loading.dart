import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gredu_teacher/app/base/resource/_index.dart';
import 'package:velocity_x/velocity_x.dart';

class ExUiLoading extends StatelessWidget {
  const ExUiLoading({
    Key? key,
    this.message,
    this.pbColor,
    this.pbSize,
  }) : super(key: key);

  final String? message;
  final Color? pbColor;
  final double? pbSize;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SpinKitFadingCube(color: pbColor ?? colorWhite, size: pbSize ?? 24),
          "${message ?? "Sedang mengambil data ..."}".text.make()
        ],
      ),
    );
  }
}

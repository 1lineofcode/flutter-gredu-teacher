import 'package:flutter/cupertino.dart';
import 'package:gredu_teacher/app/base/resource/_index.dart';
import 'package:gredu_teacher/app/reusable/widgets/_index.dart';
import 'package:velocity_x/velocity_x.dart';

class ExUiError extends StatelessWidget {
  const ExUiError({
    Key? key,
    this.errorMessage,
    this.onRetry,
    this.btnColor,
  }) : super(key: key);

  final String? errorMessage;
  final Color? btnColor;
  final VoidCallback? onRetry;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SizedBox(height: context.percentHeight * 40),
          "${errorMessage ?? "Ups, Terjadi kesalahan UI"}"
              .text
              .white
              .center
              .makeCentered(),
          SizedBox(height: 8),
          ExButtonOutline(
            color: btnColor ?? colorWhite,
            btnText: "Retry",
            onPress: () => onRetry,
          )
        ],
      ).p24(),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import '../../repository/remote/_index.dart';
import '../../repository/local/_index.dart';
import 'common_widgets.dart';
export 'package:get/get.dart';

///   created               : Aditya Pratama
///   originalFilename      : base_controller
///   date                  : 11 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///  abstraction class from [GetxController]
///  write all methods which will be used on controller !

abstract class BaseController extends GetxController with CommonWidgets {

  // REMOTE REPO
  var repoAuth = AuthRepositories();
  var repoAssessment = AssessmentRepositories();
  var repoAttendance = AttendanceRepositories();
  var repoCounseling = CounselingRepositories();
  var repoFilter = FilterRepositories();
  var repoInbox = InboxRepositories();
  var repoLesson = LessonRepositories();
  var repoNotification = NotificationRepositories();
  var repoSchedule = ScheduleRepositories();
  var repoSchool = SchoolRepositories();
  var repoScore = ScoreRepositories();
  var repoUpload = UploadRepositories();
  var repoUser = UserRepositories();

  // LOCAL REPO
  var localRepoAuth = LocalAuthRepositories();



  // COMMON VARIABLE
  bool isLoadMore = false;

  Map<String, String> headersImageReferer = {
    'Referer' : '${dotenv.env['IMAGE_REFERER']}'
  };

  // COMMON FUNCTION
  void onRefresh() {}
  void onLoadMore() {}
}

import 'dart:ui';

///   created               : Aditya Pratama
///   originalFilename      : color
///   date                  : 10 Jun 2021
///   —————————————————————————————————————————————————————————————————————————————
///   guide line            : https://app.zeplin.io/project/5e425d9ebb5b28b3f8b34436/styleguide/colors

const colorWhite = Color(0xFFFFFFFF);
const colorBlack = Color(0xFF222222);
const colorPrimary = Color(0xFF72a065);
const colorPrimaryDark = Color(0xff5e8453);
const colorPrimaryLight = Color(0xffdae8d2);
const colorAccent = Color(0xFFff6d4a);
const colorAccentDark = Color(0xffdc593a);
const colorAccentLight = Color(0xfff58c72);
const scaffoldBackgroundColor = Color.fromRGBO(245, 247, 249, 1);
// —————————————————————————————————————————————————————————————————————————————
const paleGrey = Color(0xFFe4e8eb);
const containerGrey = Color(0xFFbababa);
const darkSlateBlue10 = Color(0x1a223f5e);
const battleshipGrey = Color(0xFF62717a);
const whiteGrey = Color(0xFFe0e0e0);
const lightSage = Color(0xFFedf7e7);
const pinkishOrange = Color(0xFFff6d4a);

// dark theme !!
const primaryDarkColor =  Color(0xFF250048);
const darkBackgroundColor = Color(0xFF000000);
const iconColorDark = colorWhite;
const textColorDark =  Color(0xFFffffff);
const buttonColorDark =  primaryDarkColor;
const textButtonColorDark =  colorBlack;
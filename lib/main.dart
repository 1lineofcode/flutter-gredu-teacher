import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';

import 'app/base/resource/_index.dart';
import 'app/routes/app_pages.dart';


Future main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();

  // .env
  await FlutterConfig.loadEnvVariables();
  if(kReleaseMode == false) await dotenv.load(fileName: ".env.development");
  else await dotenv.load(fileName: ".env.production");


  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    printInfo(info: dotenv.env['BASE_URL'] ?? "NULL");
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppThemes.theme(),
      darkTheme: AppThemes.darktheme(),
      themeMode: AppThemes().init(),
      locale: AppTranslations.locale,
      fallbackLocale: AppTranslations.fallbackLocale,
      translations: AppTranslations(),
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    );
  }
}
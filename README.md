## Getting Started

this project generate by **Flutter 2** supports **null safety** and using **GetX Ecosystem**. Flutter 2.2.1 & Dart 2.13.1  
Project Design Pattern : [screenshot](https://prnt.sc/159ernr)      

### LEARN REFERENCE
- **getX** https://github.com/jonataslaw/getx#about-get
- **velocityX** https://velocityx.dev/

---

### NEED TO INSTALL
 
 - **first time open project** : `flutter pub get`
 
 - **get_cli** : `pub global activate get_cli`  
 
  - **plug-in getXSnippet** (An extension to accelerate the process of developing applications with flutter, aimed at everyone using the GetX package.)  
 
  - **plug-in JsonToDart** (generates Dart Model classes from JSON text. It can find inner classes in nested JSON and create classes in single file. Short cut key to use  *Option + Shift + D*  

---  
  
### TIPS WHEN DEVELOP
- To **create a new page** with get_cli :  `get create page:test_page`

- To **create a model class** with JsonToDart : `Option + Shift + D` copy and paste all response to GUI. 
  screenshot : [screenshot](https://prnt.sc/159bp76)

- Always extend controller class to [BaseController]

---  

### Project Tree
```editorconfig
├── app
│   ├── base
│   │   ├── exception
│   │   ├── lifecycle
│   │   ├── networking
│   │   └── resource
│   ├── constants
│   ├── model
│   ├── module
│   │   ├── bindings
│   │   ├── controller
│   │   └── view
│   ├── repository
│   ├── reusable
│   │   ├── utils
│   │   └── widgets
│   └── routes
├── .env.development
├── .env.production
└── main.dart
```
